<?php

class User_m extends CI_Model
{
    public function login($post)
    {

        $username = $post['username'];
        $password = $post['password'];
        $query = $this->db->query("SELECT * FROM user WHERE username = '$username'");
        $row = $query->row();
        if ($query->num_rows() > 0) {
            if (password_verify($password, $row->password)) {
                return $query;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function get($id = null)
    {
        $this->db->from('user');
        $this->db->join('jabatan', 'user.id_jabatan = jabatan.id_jabatan');
        if ($id != null) {
            $this->db->where('id_user', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        // $params['id'] = null;
        $params['nip'] = $post['nip'];
        $params['nama'] = $post['fullname'];
        $params['id_jabatan'] = $post['jabatan'];
        $params['jenis_kelamin'] = $post['jenis_kelamin'];
        $params['tempat_tgl_lahir'] = $post['ttl'];
        $params['username'] = $post['username'];
        $params['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        $params['alamat'] = $post['alamat'];
        $params['level'] = $post['level'];

        $this->db->insert('user', $params);
    }

    public function edit($post)
    {
        $params['nip'] = $post['nip'];
        $params['nama'] = $post['fullname'];
        $params['id_jabatan'] = $post['jabatan'];
        $params['jenis_kelamin'] = $post['jenis_kelamin'];
        $params['tempat_tgl_lahir'] = $post['ttl'];
        $params['username'] = $post['username'];
        if(!empty($post['password']))
        {
            $params['password'] = password_hash($post['password'], PASSWORD_DEFAULT);
        }
        $params['alamat'] = $post['alamat'];
        $params['level'] = $post['level'];

        $this->db->where('id_user', $post['user_id']);
        $this->db->update('user', $params);
    }

    public function del($id)
    {
        $this->db->where('id_user', $id);
        $this->db->delete('user');
    }
}
