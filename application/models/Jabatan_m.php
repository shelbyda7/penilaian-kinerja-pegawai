<?php 

class Jabatan_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('jabatan');
        if ($id != null) {
            $this->db->where('id_jabatan', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        $params = [
            'nama_jabatan' =>$post['nama_jabatan']
        ];
        $this->db->insert('jabatan', $params);
    }

    public function edit($post)
    {
        $params = [
            'nama_jabatan' => $post['nama_jabatan']
        ];
        $this->db->where('id_jabatan', $post['id_jabatan']);
        $this->db->update('jabatan', $params);
    }

    public function del($id)
    {
        $this->db->where('id_jabatan', $id);
        $this->db->delete('jabatan');
    }
}
?>