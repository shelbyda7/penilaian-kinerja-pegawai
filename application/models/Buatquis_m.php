<?php 

class Buatquis_m extends CI_Model
{
    public function get($id = null)
    {
        $this->db->from('quis');
        if ($id != null) {
            $this->db->where('id_quis', $id);
        }
        $query = $this->db->get();
        return $query;
    }

    public function add($post)
    {
        $params = [
            'title' =>$post['title']
        ];
        $this->db->insert('quis', $params);
    }

    public function edit($post)
    {
        $params = [
            'title' => $post['title']
        ];
        $this->db->where('id_quis', $post['id_quis']);
        $this->db->update('quis', $params);
    }

    public function del($id)
    {
        $this->db->where('id_quis', $id);
        $this->db->delete('quis');
    }
}
?>