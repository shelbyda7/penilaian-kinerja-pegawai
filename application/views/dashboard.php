<section class="content-header">
    <h1>
        Dashboard
        <small>Preview sample</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url('dashboard') ?>"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <i class="fa fa-bar-chart-o"></i>
                    <h3 class="box-title">Bar Chart</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="myChart" style="height:300px"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content-header">
    <h1 class="text-center" style="font-weight: bold;">
        Peringkat Kinerja Karyawan
    </h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Kepala Departement</h3>
                </div>
                <div class="box-body">
                    <table id="table1" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Peringkat</th>
                                <th>Nama</th>
                                <th>Total Nilai</th>
                                <th>Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>123456</td>
                                <td>Andika Eka Wardana</td>
                                <td>100</td>
                                <td>S++</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Manager</h3>
                    </div>
                    <div class="box-body">
                        <table id="table2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Peringkat</th>
                                    <th>Nama</th>
                                    <th>Total Nilai</th>
                                    <th>Grade</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>123456</td>
                                    <td>Andika Eka Wardana</td>
                                    <td>100</td>
                                    <td>S++</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Supervisor</h3>
                </div>
                <div class="box-body">
                    <table id="table3" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Peringkat</th>
                                <th>Nama</th>
                                <th>Total Nilai</th>
                                <th>Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>123456</td>
                                <td>Andika Eka Wardana</td>
                                <td>100</td>
                                <td>S++</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Sales</h3>
                </div>
                <div class="box-body">
                    <table id="table4" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Peringkat</th>
                                <th>Nama</th>
                                <th>Total Nilai</th>
                                <th>Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>123456</td>
                                <td>Andika Eka Wardana</td>
                                <td>100</td>
                                <td>S++</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- jQuery 2.2.3 -->
<script src="<?= base_url(); ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url(); ?>asset/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url(); ?>asset/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url(); ?>asset/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>asset/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>asset/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>asset/dist/js/demo.js"></script>
<!-- FLOT CATEGORIES PLUGIN - Used to draw bar charts -->
<script src="<?= base_url() ?>asset/plugins/chartjs/Chart.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table1').DataTable()
    })
    $(document).ready(function() {
        $('#table2').DataTable()
    })
    $(document).ready(function() {
        $('#table3').DataTable()
    })
    $(document).ready(function() {
        $('#table4').DataTable()
    })
</script>

<script>
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
            datasets: [{
                label: 'Data Penjualan Perbulan',
                data: [12, 19, 3, 23, 2, 3, 25, 10, 20, 13, 7, 17],
                backgroundColor: [
                    // 'rgba(255, 99, 132, 0.2)',
                    // 'rgba(54, 162, 235, 0.2)',
                    // 'rgba(255, 206, 86, 0.2)',
                    // 'rgba(75, 192, 192, 0.2)',
                    // 'rgba(153, 102, 255, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)',
                    // 'rgba(255, 159, 64, 0.2)'
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)',
                    'rgba(60, 141, 188, 0.4)'
                ],
                borderColor: [
                    // 'rgba(255,99,132,1)',
                    // 'rgba(54, 162, 235, 1)',
                    // 'rgba(255, 206, 86, 1)',
                    // 'rgba(75, 192, 192, 1)',
                    // 'rgba(153, 102, 255, 1)',
                    // 'rgba(255, 159, 64, 1)',
                    // 'rgba(255, 159, 64, 1)',
                    // 'rgba(255, 159, 64, 1)',
                    // 'rgba(255, 159, 64, 1)',
                    // 'rgba(255, 159, 64, 1)',
                    // 'rgba(255, 159, 64, 1)'
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)',
                    'rgba(255, 140, 0, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
</script>

</body>

</html>