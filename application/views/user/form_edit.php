<section class="content-header">
    <h1>Users
        <small>Pengguna</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Users</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit User</h3>
            <div class="pull-right">
                <a href="<?= site_url('user') ?>" class="btn btn-warning btn-flat">
                    <i class="fa fa-undo"></i>Back
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="" method="post">
                        <div class="form-group <?= form_error('nip') ? 'has-error' : null ?>">
                            <label>NIP *</label>
                            <input type="hidden" name="user_id" value="<?= $row->id_user ?>">
                            <input type="text" name="nip" value="<?= $this->input->post('nip') ?? $row->nip ?>" class="form-control">
                            <?= form_error('nip'); ?>
                        </div>
                        <div class="form-group <?= form_error('fullname') ? 'has-error' : null ?>">
                            <label>Nama *</label>
                            <input type="text" name="fullname" value="<?= $this->input->post('fullname') ?? $row->nama ?>" class="form-control">
                            <?= form_error('fullname'); ?>
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin *</label>
                            <?php $jenis_kelamin = $this->input->post('jenis_kelamin') ?? $row->jenis_kelamin ?>
                            <div class="form-group">
                                <input type="radio" id="jenis_kelamin" name="jenis_kelamin" value="laki-laki" class="form-radio" <?= $jenis_kelamin == 'laki-laki' ? "checked" : null ?>> Laki-Laki
                            </div>
                            <div class="form-group">
                                <input type="radio" id="jenis_kelamin" name="jenis_kelamin" value="perempuan" class="form-radio" <?= $jenis_kelamin == 'perempuan' ? "checked" : null ?>> Perempuan
                            </div>
                        </div>
                        <div class="form-group <?= form_error('username') ? 'has-error' : null ?>">
                            <label>Username *</label>
                            <input type="text" name="username" value="<?= $this->input->post('username') ?? $row->username ?>" class="form-control">
                            <?= form_error('username'); ?>
                        </div>
                        <div class="form-group <?= form_error('password') ? 'has-error' : null ?> ">
                            <label for="password">Password *</label>
                            <input type="password" id="password" name="password" value="<?= set_value('password') ?>" class="form-control">
                            <?= form_error('password') ?>
                        </div>
                        <div class="form-group <?= form_error('confpassword') ? 'has-error' : null ?> ">
                            <label for="confpassword">Konfirmasi Password *</label>
                            <input type="password" id="confpassword" name="confpassword" value="<?= set_value('confpassword') ?>" class="form-control">
                            <?= form_error('confpassword') ?>
                        </div>
                        <div class="form-group <?= form_error('jabatan') ? 'has-error' : null ?>">
                            <label for="jabatan">Jabatan *</label>
                            <select class="form-control" name="jabatan" id="jabatan">
                                <option value="">== Pilih ==</option>
                                <?php
                                    foreach ($jabatan as $item) { ?>
                                        <option value="<?= $item->id_jabatan ?>" <?=$item->id_jabatan == $row->id_jabatan ? "selected" : null ?>><?= $item->nama_jabatan ?></option>
                                   <?php }
                                ?>
                            </select>
                            <?= form_error('jabatan') ?>
                        </div>
                        <div class="form-group <?= form_error('alamat') ? 'has-error' : null ?> ">
                            <label for="alamat">Alamat *</label>
                            <textarea type="text" id="alamat" name="alamat" value="" class="form-control"><?= $this->input->post('alamat') ?? $row->alamat ?></textarea>
                            <?= form_error('alamat') ?>
                        </div>
                        <div class="form-group <?= form_error('ttl') ? 'has-error' : null ?> ">
                            <label for="ttl">Tanggal Lahir *</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" id="ttl" name="ttl" value="<?= $this->input->post('ttl') ?? $row->tempat_tgl_lahir ?>" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </div>
                            <?= form_error('ttl') ?>
                        </div>
                        <div class="form-group <?= form_error('level') ? 'has-error' : null ?> ">
                            <label for="level">Level *</label>
                            <select class="form-control" name="level" id="level">
                            <?php $level = $this->input->post('level') ?? $row->level ?>
                                <option value="">== Pilih ==</option>
                                <option value="1" <?= $level == 1 ? "selected" : null ?>>Super User</option>
                                <option value="2" <?= $level == 2 ? "selected" : null ?>>Pengguna</option>
                            </select>
                            <?= form_error('level') ?>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-paper-plane"></i>Save</button>
                            <button type="reset" class="btn btn-flat">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>