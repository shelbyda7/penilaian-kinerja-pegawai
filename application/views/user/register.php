<section class="content-header">
    <h1>
        Registrasi User
        <small>Priview Registrasi New</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="<?= base_url('user/user_data') ?>"><i class="fa fa-users"></i> User</a></li>
        <li>Registrasi</li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Registrasi Form</h3>
            <div class="pull-right">
                <a href="<?= base_url('user')?>" class="btn btn-warning btn-flat">
                    <i class="fa fa-undo"></i> Back
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <form action="<?= site_url('user/add') ?>" method="post">
                        <div class="form-group <?= form_error('nip') ? 'has-error' : null ?>">
                            <label for="nip">NIP (Nomor Induk Pegawai) *</label>
                            <!-- <input type="hidden" name="user_id" value=""> -->
                            <input type="number" id="nip" name="nip" value="<?= set_value('nip') ?>" class="form-control">
                            <?= form_error('nip') ?>
                        </div>
                        <div class="form-group <?= form_error('fullname') ? 'has-error' : null ?> ">
                            <label for="fullname">Nama Lengkap *</label>
                            <input type="text" id="fullname" name="fullname" value="<?= set_value('fullname') ?>" class="form-control">
                            <?= form_error('fullname') ?>
                        </div>
                        <div class="form-group">
                            <label for="jenis_kelamin">Jenis Kelamin *</label>
                            <div class="form-group">
                                <input type="radio" id="jenis_kelamin" name="jenis_kelamin" value="laki-laki" class="form-radio" checked> Laki-Laki
                            </div>
                            <div class="form-group">
                                <input type="radio" id="jenis_kelamin" name="jenis_kelamin" value="perempuan" class="form-radio"> Perempuan
                            </div>
                        </div>
                        <div class="form-group <?= form_error('username') ? 'has-error' : null ?> ">
                            <label for="username">Username *</label>
                            <input type="text" id="username" name="username" value="<?= set_value('username') ?>" class="form-control">
                            <?= form_error('username') ?>
                        </div>
                        <div class="form-group <?= form_error('password') ? 'has-error' : null ?> ">
                            <label for="password">Password *</label>
                            <input type="password" id="password" name="password" value="<?= set_value('password') ?>" class="form-control">
                            <?= form_error('password') ?>
                        </div>
                        <div class="form-group <?= form_error('confpassword') ? 'has-error' : null ?> ">
                            <label for="confpassword">Konfirmasi Password *</label>
                            <input type="password" id="confpassword" name="confpassword" value="<?= set_value('confpassword') ?>" class="form-control">
                            <?= form_error('confpassword') ?>
                        </div>
                        <div class="form-group <?= form_error('jabatan') ? 'has-error' : null ?> ">
                            <label for="jabatan">Jabatan *</label>
                            <select class="form-control" name="jabatan" id="jabatan">
                                <option value="">== Pilih ==</option>
                                <?php
                                    foreach ($jabatan as $item) { ?>
                                        <option value="<?= $item->id_jabatan ?>"><?= $item->nama_jabatan ?></option>
                                   <?php }
                                ?>
                            </select>
                            <?= form_error('jabatan') ?>
                        </div>
                        <div class="form-group <?= form_error('alamat') ? 'has-error' : null ?> ">
                            <label for="alamat">Alamat Lengkap *</label>
                            <textarea type="text" id="alamat" name="alamat" value="" class="form-control"><?= set_value('alamat') ?></textarea>
                            <?= form_error('alamat') ?>
                        </div>
                        <div class="form-group <?= form_error('ttl') ? 'has-error' : null ?> ">
                            <label for="ttl">Tanggal Lahir *</label>

                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="date" id="ttl" name="ttl" value="<?= set_value('ttl') ?>" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                            </div>
                            <?= form_error('ttl') ?>
                        </div>
                        <div class="form-group <?= form_error('level') ? 'has-error' : null ?> ">
                            <label for="level">Level *</label>
                            <select class="form-control" name="level" id="level">
                                <option value="">== Pilih ==</option>
                                <option value="1" <?= set_value('level') == 1 ? "selected" : null ?>>Super User</option>
                                <option value="2" <?= set_value('level') == 2 ? "selected" : null ?>>Pengguna</option>
                            </select>
                            <?= form_error('level') ?>
                        </div>
                        <div class="form-group">
                            <button type="submit" name="simpan" " class=" btn btn-success btn-flat"><i class="fa fa-paper-plane"></i> Save</button>
                            <button type="reset" class="btn btn-flat"> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>