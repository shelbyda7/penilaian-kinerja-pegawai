<section class="content-header">
    <h1>
        Penilaian
        <small>Rate us karyawan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#"><i class="fa fa-tasks"></i> Penilaian</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Nilai Karyawan</h3>
            <div class="pull-right">
                <a href="<?= site_url('dashboard') ?>" class="btn btn-warning btn-flat">
                    <i class="fa fa-undo"></i> Back
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="box-body pad">
                    <form action="" method="post">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="form-group">
                                <label for="nip">Nama Penilai *</label>
                                <input type="number" id="nip" name="nip" value="" class="form-control" readonly>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Jabatan yang ingin dinilai *</label>
                                <select class="form-control select" name="" id="jabatan" required>
                                    <option value="">== Pilih ==</option>
                                    <option value="">dummy</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="jabatan">Nama Pegawai *</label>
                                <select class="form-control select" name="" id="pegawai" required>
                                    <option value="">== Pilih ==</option>
                                    <option value="">dummy</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="nip">NIP (Nomor Induk Pegawai) *</label>
                                <input type="number" id="nip" name="nip" value="" class="form-control" readonly>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    <h4><b>Berikan Nilai Anda *</b></h4>
                                </label>
                                <!-- <input type="hidden" name="id_quis" value=""></input> -->
                                <table id="table1" class="table table-bordered table-striped">
                                    <thead class="callout callout-success">
                                        <tr>
                                            <th style="width:5px">No</th>
                                            <th>Deskripsi</th>
                                            <th style="width:220px">Point</th>
                                        </tr>
                                    </thead>
                                    <!-- <?php  ?> -->
                                    <tbody>
                                        <tr>
                                            <td class="text-center">1</td>
                                            <td>isi quis</td>
                                            <td>
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="point" class="minimal" value="0">&nbsp;<b>1</b>&nbsp;&nbsp;
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="point" class="minimal" value="25">&nbsp;<b>2</b>&nbsp;&nbsp;
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="point" class="minimal" value="50">&nbsp;<b>3</b>&nbsp;&nbsp;
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="point" class="minimal" value="75">&nbsp;<b>4</b>&nbsp;&nbsp;
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="point" class="minimal" value="100">&nbsp;<b>5</b>&nbsp;&nbsp;
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="form-group pull-right">
                                <button type="submit" name="simpan" class=" btn btn-success btn-flat"><i class="fa fa-paper-plane"></i> Save</button>
                                <button type="reset" class="btn btn-flat"> Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- jQuery 2.2.3 -->
<script src="<?= base_url() ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url() ?>asset/bootstrap/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?= base_url() ?>asset/plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?= base_url() ?>asset/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= base_url() ?>asset/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= base_url() ?>asset/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="<?= base_url() ?>asset/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?= base_url() ?>asset/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- bootstrap color picker -->
<script src="<?= base_url() ?>asset/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?= base_url() ?>asset/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?= base_url() ?>asset/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?= base_url() ?>asset/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url() ?>asset/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url() ?>asset/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url() ?>asset/dist/js/demo.js"></script>
<!-- Page script -->
<script type="text/javascript">
    $(document).ready(function() {
        //Initialize Select2 Elements
        $(".select").select2();
    });
</script>