<section class="content-header">
    <h1>
        Laporan Penilaian Manager
        <small>List Penilaian Kinerja Karyawan</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="#"><i class="fa fa-files-o"></i> Laporan Penilaian</a></li>
        <li class="active"><a href="#"><i class="fa fa-paper-plane-o"></i> Manager</a></li>
    </ol>
</section>
<section class="content">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Hasil Penilaian </h3>
        </div>
        <div class="box-body">
            <table id="table1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th style="width:5px">No</th>
                        <th>Date</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>Alamat</th>
                        <th style="width:10px">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>dummy</td>
                        <td>dummmy</td>
                        <td>dummy</td>
                        <td>dummy</td>
                        <td class="text-center" width="160px">
                            <a href="" class="btn btn-success btn-xs">
                                <i class="fa fa-file-excel-o"></i>&nbsp; Export To Excel
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</section>
<!-- jQuery 2.2.3 -->
<script src="<?= base_url(); ?>asset/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?= base_url(); ?>asset/bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?= base_url(); ?>asset/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url(); ?>asset/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url(); ?>asset/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?= base_url(); ?>asset/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url(); ?>asset/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= base_url(); ?>asset/dist/js/demo.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#table1').DataTable()
    })
</script>

</body>

</html>