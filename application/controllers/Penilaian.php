<?php

class Penilaian extends CI_Controller
{

    public function kepala_departement()
    {
        // $data['row'] = $this->buatquis_m->get()->result();
        $data['judul'] = 'Laporan Penilaian Kinerja Karyawan';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'laporan-penilaian/kepala_departement');
    }

    public function manager()
    {
        // $data['row'] = $this->buatquis_m->get()->result();
        $data['judul'] = 'Laporan Penilaian Kinerja Karyawan';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'laporan-penilaian/manager');
    }

    public function supervisor()
    {
        // $data['row'] = $this->buatquis_m->get()->result();
        $data['judul'] = 'Laporan Penilaian Kinerja Karyawan';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'laporan-penilaian/supervisor');
    }

    public function sales()
    {
        // $data['row'] = $this->buatquis_m->get()->result();
        $data['judul'] = 'Laporan Penilaian Kinerja Karyawan';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'laporan-penilaian/sales');
    }
}
