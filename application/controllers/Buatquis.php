<?php

class Buatquis extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('buatquis_m');
    }

    public function index()
    {
        $data['row'] = $this->buatquis_m->get()->result();
        $data['judul'] = 'Buat Quisioner';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'buatquisioner/buatquis_data');
    }

    public function add()
    {
        $buatquis = new stdClass();
        $buatquis->id_quis = null;
        $buatquis->title = null;

        $data = array(
            'page'  => 'add',
            'row'   => $buatquis
        );
        $data['judul'] = 'Buat Quisioner';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'buatquisioner/buatquis_form');
        // $this->load->view('template/footer');
    }

    public function edit($id)
    {
        $query = $this->buatquis_m->get($id);
        if ($query->num_rows() > 0) {
            $buatquis = $query->row();
            $data = array(
                'page'  => 'edit',
                'row'   => $buatquis
            );
            $data['judul'] = 'Buat Quisioner';
            $this->load->view('template/header', $data);
            $this->template->load('template', 'buatquisioner/buatquis_form');
            // $this->load->view('template/footer');
        }
    }

    public function process()
    {
        $post = $this->input->post(null, TRUE);
        if (isset($post['add'])) {
            $this->buatquis_m->add($post);
        } elseif (isset($post['edit'])) {
            $this->buatquis_m->edit($post);
        }
        echo "<script>window.location='" . site_url('buatquis') . "';</script>";
    }

    public function del($id)
    {
        $this->buatquis_m->del($id);
        echo "<script>window.location='" . site_url('buatquis') . "';</script>";
    }
}
