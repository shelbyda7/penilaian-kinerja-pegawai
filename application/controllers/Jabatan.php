<?php

class Jabatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('jabatan_m');
    }

    public function index()
    {
        $data['row'] = $this->jabatan_m->get()->result();
        $data['judul'] = 'Jabatan Pegawai';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'jabatan/jabatan_data');
        // $this->load->view('template/footer');
    }

    public function add()
    {
        $jabatan = new stdClass();
        $jabatan->id_jabatan = null;
        $jabatan->nama_jabatan = null;

        $data = array(
            'page'  => 'add',
            'row'   => $jabatan
        );
        $data['judul'] = 'Jabatan Pegawai';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'jabatan/jabatan_form');
        $this->load->view('template/footer');
    }

    public function edit($id)
    {
        $query = $this->jabatan_m->get($id);
        if ($query->num_rows() > 0) {
            $jabatan = $query->row();
            $data = array(
                'page'  => 'edit',
                'row'   => $jabatan
            );
            $data['judul'] = 'Jabatan Pegawai';
            $this->load->view('template/header', $data);
            $this->template->load('template', 'jabatan/jabatan_form');
            $this->load->view('template/footer');
        }
    }

    public function process()
    {
        $post = $this->input->post(null, TRUE);
        if (isset($post['add'])) {
            $this->jabatan_m->add($post);
        } elseif (isset($post['edit'])) {
            $this->jabatan_m->edit($post);
        }
        echo "<script>window.location='" . site_url('jabatan') . "';</script>";
    }

    public function del($id)
    {
        $this->jabatan_m->del($id);
        echo "<script>window.location='" . site_url('jabatan') . "';</script>";
    }
}
