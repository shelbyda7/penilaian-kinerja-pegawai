<?php

class User extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        not_login();
        $this->load->helper(array('form', 'url'));
        $this->load->model(['user_m', 'jabatan_m']);
    }

    public function index()
    {
        $data['result'] = $this->user_m->get()->result();
        $data['judul'] = 'Data User';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'user/user_data');
    }

    public function register()
    {
        $data['judul'] = 'Registrasi User';
        $data['jabatan'] = $this->jabatan_m->get()->result();
        $this->load->view('template/header', $data);
        $this->template->load('template', 'user/register');
        $this->load->view('template/footer');
    }

    public function add()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nip', 'NIP', 'required|min_length[5]|is_unique[user.nip]');
        $this->form_validation->set_rules('fullname', 'Nama', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|is_unique[user.username]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        $this->form_validation->set_rules(
            'confpassword',
            'Konfirmasi Password',
            'required|matches[password]',
            array('matches' => '%s tidak sesuai dengan password')
        );
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('ttl', 'Tempat Tanggal Lahir', 'required');
        $this->form_validation->set_rules('level', 'Roles User', 'required');
        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('min_length', '%s minimal 5 karakter');
        $this->form_validation->set_message('is_unique', '%s ini sudah dipakai, silahkan ganti');
        $this->form_validation->set_error_delimiters('<span class="help-block" >' . '</span>');

        if ($this->form_validation->run() == FALSE) {
            $data['judul'] = 'Registrasi User';
            $this->load->view('template/header', $data);
            $this->template->load('template', 'user/register');
            $this->load->view('template/footer');
        } else {
            $post = $this->input->post(null, TRUE);
            $this->user_m->add($post);
            if ($this->db->affected_rows() > 0) {
                echo "<script>
                alert('Berhasil, Data telah ditambahkan');
                window.location='" . site_url('user/index') . "';  
                </script>";
            } else {
                echo "<script>
                alert('Gagal, Data gagal ditambahkan');
                window.location='" . site_url('user/register') . "';  
                </script>";
            }
        }
    }

    public function edit($id)
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('nip', 'NIP', 'required|min_length[5]');
        $this->form_validation->set_rules('fullname', 'Nama', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
        if ($this->input->post('password')) {
            $this->form_validation->set_rules('password', 'Password', 'min_length[5]');
            $this->form_validation->set_rules(
                'confpassword',
                'Konfirmasi Password',
                'matches[password]',
                array('matches' => '%s Tidak Sesuai Dengan Password Anda')
            );
        }
        if ($this->input->post('passconf')) {
            $this->form_validation->set_rules(
                'confpassword',
                'Konfirmasi Password',
                'matches[password]',
                array('matches' => '%s Tidak Sesuai Dengan Password Anda')
            );
        }
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
        $this->form_validation->set_rules('ttl', 'Tempat Tanggal Lahir', 'required');
        $this->form_validation->set_rules('level', 'Roles User', 'required');
        $this->form_validation->set_message('required', '%s masih kosong, silahkan isi');
        $this->form_validation->set_message('min_length', '%s minimal 5 karakter');
        $this->form_validation->set_message('is_unique', '%s ini sudah dipakai, silahkan ganti');
        $this->form_validation->set_error_delimiters('<span class="help-block" >' . '</span>');
        if ($this->form_validation->run() == FALSE) {
            $query = $this->user_m->get($id);
            if ($query->num_rows() > 0) {
                $data['jabatan'] = $this->jabatan_m->get()->result();
                $data['row'] = $query->row();
                $data['judul'] = 'Registrasi User';
                $this->load->view('template/header', $data);
                $this->template->load('template', 'user/form_edit');
                $this->load->view('template/footer');
            } else {
                echo "<script>alert('Data Tidak Ditemukan');";
                echo "window.location='" . site_url('user') . "';</script>";
            }
        } else {
            $post = $this->input->post(null, FALSE);
            $this->user_m->edit($post);
            if ($this->db->affected_rows() > 0) {
                echo "<script>alert('Data Berhasil Disimpan');</script>";
            }
            echo "<script>window.location='" . site_url('user') . "';</script>";
        }
    }

    public function del($id)
    {
        $this->user_m->del($id);
        if ($this->db->affected_rows() > 0) {
            $this->session->set_flashdata('success', 'Data Berhasil Dihapus');
        }
        echo "<script>window.location='" . site_url('user') . "';</script>";
    }
}
