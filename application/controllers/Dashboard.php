<?php
class Dashboard extends CI_Controller
{
    public function index()
    {
        not_login();
        $data['judul'] = 'Dashboard PHP';
        $this->load->view('template/header', $data);
        $this->template->load('template', 'dashboard');
        // $this->load->view('template/footer');
    }
}
