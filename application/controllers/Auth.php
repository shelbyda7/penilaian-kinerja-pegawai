<?php

class Auth extends CI_Controller
{
    public function login()
    {
        already_login();
        $this->load->view('login');
    }

    public function process()
    {
        $post = $this->input->post(null, TRUE);
        if (isset($post['login'])) {
            $this->load->model('user_m');
            $query = $this->user_m->login($post);
            if ($query == true) {
                $row = $query->row();
                $params = array(
                    'userid' => $row->id_user,
                    'level' => $row->level
                );
                $this->session->set_userdata($params);
                echo "<script>
                    alert('Selamat, login berhasil');
                    window.location='" . site_url('dashboard') . "';    
                </script>";
            } else {
                echo "<script>
                    alert('Login gagal, username atau password salah');
                    window.location='" . site_url('auth/login') . "';    
                </script>";
            }
        } else {
            echo "<script>window.location='" . site_url('auth/login') . "';</script>";
        }
    }

    public function logout()
    {
        $params = array('userid', 'level');
        $this->session->unset_userdata($params);
        redirect('auth/login');
    }
}
